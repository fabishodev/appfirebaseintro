package com.example.appfirebaseintro

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val TAG = "FirebaseDebug"
        val db = FirebaseFirestore.getInstance()

        db.collection("books")
            .addSnapshotListener{ value, error ->
                value.let{
                    for (document in value?.documents!!){
                        Log.d(TAG, " Name ${document?.getString("name")}")
                        Log.d(TAG, " Description ${document?.getString("description")}")
                    }
                }
            }

       /*
        db.collection("books")
            .document("001")
            .addSnapshotListener{value,error ->
                value.let{  document ->
                    document.let{
                        Log.d(TAG, " Name ${document?.getString("name")}")
                        Log.d(TAG, " Description ${document?.getString("description")}")

                    }
                }
            }*/


        /*db.collection("books")
            .document()
            .set(Book(name = "Programming in Go", description = "Learn Go"))
            .addOnSuccessListener {
                Log.d(TAG, "Book Saved")
            }
            .addOnFailureListener { exeption ->
                Log.d(TAG, "An error ocurred")
            }*/

        /*Log.d(TAG, "Get document by Property")
        db.collection("books")
           .whereEqualTo("name","Programming in C++")
           .get()
           .addOnSuccessListener {result ->
               for(document in result){
                   Log.d(TAG, " Name ${document.getString("name")}")
                   Log.d(TAG, " Description ${document.getString("description")}")
               }

           }*/


        /*Log.d(TAG, "Get document by ID")
        db.collection("books")
           .document("001")
           .get()
           .addOnSuccessListener {document ->
               document.let{
                   Log.d(TAG, " Name ${document.getString("name")}")
                   Log.d(TAG, " Description ${document.getString("description")}")
               }
           }
           .addOnFailureListener{
           }*/

        /*Log.d(TAG, "Get all documents")
        db.collection("books")
           .get()
           .addOnSuccessListener {result ->
               for(document in result){
                   Log.d(TAG, " Name ${document.getString("name")}")
                   Log.d(TAG, " Description ${document.getString("description")}")
               }
           }
           .addOnFailureListener{
           }*/
    }

    data class Book(var name:String = "", var description:String = "")
}